/**
 * imagesResize
 * Resize all JPG images to three different sizes: 200, 500, and 630 pixels
 */
const imagesResize = {
  '*.{jpg,png,jpeg,gif,webp}': [{
    crop: true,
    width: 350,
    height: 350,
    gravity: 'Center',
    suffix: '-thumb',
    upscale: true
  }, {
    width: 850,
    suffix: '-full'
  }],
  'slides/*.{jpg,png,jpeg,gif,webp}': [{
    crop: true,
    width: 1300,
    height: 430,
    gravity: 'Center',
    suffix: '-full',
    upscale: true
  }],
  'mapenal/*.{jpg,png,jpeg,gif,webp}': [{
    crop: true,
    width: 420,
    height: 300, // Cocina 256, Baños 300
    gravity: 'North',
    suffix: '-420',
    upscale: true
  }]
};

module.exports = imagesResize;