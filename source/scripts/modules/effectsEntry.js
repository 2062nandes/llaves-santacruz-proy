import { TweenLite } from 'gsap'

export const effectsEntry = () => {
  /* LOGOTIPO PRINCIPAL */
  const llaves = document.querySelector("svg #LLAVES").children
  let delayLlaves = 0.1
  for (let i = 0; i < llaves.length; i++) {
    TweenLite.fromTo(llaves[i], 2, { css: { transform: 'rotateX(-90deg)', opacity: 0 }}, { css: { transform: 'perspective(2500px) rotateX(0deg)', opacity: 1}, delay: delayLlaves })
    delayLlaves = delayLlaves + 0.3
  }
  // mascota
  TweenLite.fromTo('#mascota', 1,{x: -900},{x: 0, delay: 1}) 
  // Copias express
  TweenLite.fromTo('#line-ce', 1,{x: -900},{x: 0 , delay: 1})
  /* MENU INICIO */
  const menuInicio = document.querySelector(".menu-inicio").children
  TweenLite.fromTo(menuInicio[2], 1.2, { opacity: 0, transform: 'translateY(-100%) rotateX(-90deg)' }, { css: { opacity: 1, transform: 'perspective(2500px) rotateX(0deg) translateY(0)' }, delay: 0.5 })
  TweenLite.fromTo(menuInicio[1], 1.2, { opacity: 0, transform: 'translateY(-100%) rotateX(-90deg)' }, { css: { opacity: 1, transform: 'perspective(2500px) rotateX(0deg) translateY(0)' }, delay: 0.75 })
  TweenLite.fromTo(menuInicio[0], 1.2, { opacity: 0, transform: 'translateY(-100%) rotateX(-90deg)' }, { css: { opacity: 1, transform: 'perspective(2500px) rotateX(0deg) translateY(0)' }, delay: 1.0 })
  /* SLOGAN TEXT */
  // let slogan =  document.querySelector('.slogan')
  // const sloganSplit = [...slogan.innerHTML]
  // let delaySlogan = 0.5
  // for (let i = 0; i < sloganSplit.length; i++) {   
  //   TweenLite.fromTo(sloganSplit[i], 1, { opacity: 0, transform: 'translateX(100%) rotateY(90deg)' }, { css: { opacity: 1, transform: 'perspective(2500px) rotateY(0deg) translateX(0)' }, delay: delaySlogan })
  //   delaySlogan = delaySlogan + 0.3
  // }
  /* MENU PRINCIPAL */
  const menu = document.querySelector('ul.menu').children
  let delayMenu = 0.1
  for (let i = 0; i < menu.length; i++) {
    TweenLite.fromTo(menu[menu.length - i - 1], 1, { opacity: 0, transform: 'translateX(100%) rotateY(90deg)' }, { css: { opacity: 1, transform: 'perspective(2500px) rotateY(0deg) translateX(0)' }, delay: delayMenu })
    delayMenu = delayMenu + 0.2
  }
}
