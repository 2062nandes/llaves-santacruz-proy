export const optionSlider = {
  container: '.rex-slider',
  mode: 'gallery',
  controlsText: [
    '<i class="icon-left"></i>',
    '<i class="icon-right"></i>'
  ],
  autoplayText: [
    '<i class="icon-play"></i>',
    '<i class="icon-pause"></i>'
  ],
  speed: 2000,
  autoplay: true,
  animateIn: 'pulse',
  animateOut: 'zoomOut',
  animateDelay: '10500',
  controlsContainer: document.querySelector('.my-controls')
}
