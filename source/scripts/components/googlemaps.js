/*
  |--------------------------------------------------------------------------
  | Google Maps Javascript API
  |--------------------------------------------------------------------------
  | Add the api script of google maps with your ´YOUR_API_KEY´ before the script.js from project
  | Example Pug: ´script(src= "https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY")´
  |
  */
export const googleMap = {
  template: `<div class="google-map" :id="mapName"></div>`,
  data () {
    return {
      mapName: 'map',
      zoom: 17,
      center: {
        latitude: -17.7638500, // -17.7636239,-63.1621986
        longitude: -63.1614999
      },
      markerCoordinates: [
        {
          latitude: -17.7638900,
          longitude: -63.1612099,
          title: `<div id="content">
                      <h2 id="firstHeading" class="title--first center">SUCURSAL: LLAVES SANTA CRUZ</h2>
                      <div id="bodyContent">
                        <p><b>Descripción:</b> Copia de llaves con chip y apertura de cerraduras.</p>
                        <p><b>Sucursal:</b> Av. Lucas Saucedo, 3°Anillo interno, Mercado Mutualista, PB, Pasillo 9, Módulo 9, 
                        <br><b>Cel:</b> (+591) 75353663
                        <br><b>Email:</b> info@llavessantacruz.com</p>
                        </div><hr>
                        <!-- <footer><a class="button--cta center" href="#linkdegoogle" target="_blank">Ver en google maps</a></footer> -->
                        </div>`
        }
      ]
    }
  },
  mounted () {
    const element = document.getElementById(this.mapName)
    const options = {
      zoom: 17,
      center: new window.google.maps.LatLng(this.center.latitude, this.center.longitude)
    }
    const map = new window.google.maps.Map(element, options)
    /* MEDIA QUERIE ZOOM GOOGLE MAPS */
    const mediumBp = window.matchMedia('(min-width: 950px)')
    const changeSize = mql => {
      mql.matches
        ? map.setZoom(17)
        : map.setZoom(17)
    }
    mediumBp.addListener(changeSize)
    changeSize(mediumBp)
    this.markerCoordinates.forEach((coord) => {
      const position = new window.google.maps.LatLng(coord.latitude, coord.longitude)
      const marker = new window.google.maps.Marker({ position, map })
      const infowindow = new window.google.maps.InfoWindow()
      marker.addListener('click', function () {
        infowindow.close()
      })
      window.google.maps.event.addListener(marker, 'click',
        (function (marker, coord, infowindow) {
          return function () {
            infowindow.setContent(coord.title)
            infowindow.open(map, marker)
            map.setZoom(17)
            map.setCenter(marker.getPosition())
          }
        })(marker, coord, infowindow))
    })
  }
}
