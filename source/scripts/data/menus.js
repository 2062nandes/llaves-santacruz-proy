export const menuinicio = [
  {
    title: 'Inicio',
    href: '',
    icon: 'icon-home'
  },
  {
    title: 'Nosotros',
    href: 'nosotros.html',
    icon: 'icon-users'
  },
  {
    title: 'Contactos',
    href: 'contactos.html',
    icon: 'icon-contact'
  }
]
export const mainmenu = [
  {
    title: 'Llaves con chip',
    href: 'llaves-con-chip',
    submenu: [
      {
        title: 'Programación de llaves con chip',
        href: 'programacion-de-llaves-con-chip'
      }
    ]
  },
  {
    title: 'Copia de llaves',
    href: 'copia-de-llaves',
    submenu: [
      {
        title: 'En taller',
        href: 'en-taller'
      },
      {
        title: 'A domicilio',
        href: 'a-domicilio'
      }
    ]
  },
  {
    title: 'Duplicado de llaves para vehículos',
    href: 'duplicado-de-llaves-para-vehiculos',
    submenu: [
      {
        title: 'Autos y automotores livianos',
        href: 'autos-y-automotores-livianos'
      },
      {
        title: 'Camiones y transporte pesado',
        href: 'camiones-y-transporte-pesado'
      },
      {
        title: 'Tractores y maquinaria pesada',
        href: 'tractores-y-maquinaria-pesada'
      }
    ]
  },
  {
    title: 'Apertura de chapas y cerraduras',
    href: 'apertura-de-chapas-y-cerraduras',
    submenu: [
      {
        title: 'Autos',
        href: 'autos'
      },
      {
        title: 'Puertas de casas y departamentos',
        href: 'puertas-de-casas-y-departamentos'
      },
      {
        title: 'Puertas de garajes',
        href: 'puertas-de-garajes'
      },
      {
        title: 'Cerraduras eléctricas',
        href: 'cerraduras-electricas'
      },
      {
        title: 'Candados',
        href: 'candados'
      },
      {
        title: 'Chapas de seguridad',
        href: 'chapas-de-seguridad'
      },
      {
        title: 'Cajas fuertes',
        href: 'cajas-fuertes'
      }
    ]
  },
  {
    title: 'Cambios de combinación',
    href: 'cambios-de-combinacion',
    submenu: [
      {
        title: 'Codificación y decodificación de llaves',
        href: 'codificacion-y-decodificacion-de-llaves'
      }
    ]
  },
  {
    title: 'Instalacion y reparación de cerraduras',
    href: 'instalacion-y-reparacion-de-cerraduras',
    submenu: [
      {
        title: 'Chapas normales',
        href: 'Chapas normales'
      },
      {
        title: 'De seguridad',
        href: 'De seguridad'
      },
      {
        title: 'De alta seguridad',
        href: 'De alta seguridad'
      }
    ]
  }
]
